import firebase from 'firebase';
import { FireBaseKeys } from './keys';

export const fb = () =>firebase.initializeApp(FireBaseKeys);

export const auth = fb.auth;

export default fb;