import React, { Component } from 'react';
import Nav from '../containers/navbar/main_menu';
import $ from 'jquery';
import Login from '../containers/login/login'; 
import { connect } from 'react-redux'
import Button from '@atlaskit/button';
import { Redirect } from 'react-router-dom'

class Home extends Component {

    componentDidMount() {
        if (this.props.isSignedIn) {
            <Redirect to='/profile' />
            // let wH = $(window).height();
            // let sH = $('.signUpHPInner').height();
            // $(window).scroll(() => {
            //     let scroll = $(window).scrollTop() * 3;
            //     if (scroll > $(window).height()) {
            //         $('.signUpHP').removeClass('fadeInRight').addClass('fadeOutRight');
            //     } else {
            //         $('.signUpHP').removeClass('fadeOutRight').addClass('fadeInRight');
            //     }
            // });
        }
    }

    render() {
        return (
            <section className='h-100'>
                <Nav />
                <div className='row h-100' style={{ backgroundColor: '#ccc' }}>
                    
                    <div className='col-md-8 col-sm-12 align-self-center'>
                        <div className='h-100 '>
                            <Button appearance='primary'>Button</Button>
                        </div>
                    </div>
                    {this.props.isSignedIn ? (
                        <span></span>
                    ) : (
                            <div className='col-md-4 col-sm-12 signUpHP animated faster fadeInRight' style={{ backgroundColor: 'white' }}>
                                <div className='w-75 mx-auto h-100 signUpHPInner'>
                                    <br />
                                    <h4 className='center'>Sign Up</h4>
                                    <Login />
                                </div>
                            </div>
                        )}
                </div>

                <div className='row h-100' style={{ backgroundColor: '#ddd' }}>
                    <div className='col-md-8 col-sm-12'>
                        <div className='h-100'>
                            aaadsfsdf
                        </div>
                    </div>
                    <div className='col-md-4 col-sm-12' style={{ backgroundColor: 'red' }}>
                        <div className=''>
                            b
                        </div>
                    </div>

                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isSignedIn: state.profile.signedIn
    }
}

export default connect(mapStateToProps)(Home);