import React, { Component } from 'react';
import {GoogleKeys} from '../config/keys';
import StarRatings from 'react-star-ratings';
import Gallery from '../containers/gallery/gallery';
import FeatureBar from '../containers/featurebar/featurebar';
import Review from '../containers/review/review';
import SampleData from '../data/indsample.json';
import Nav from '../containers/navbar/main_menu'; 

class Individual extends Component {

    constructor(props) {
        super(props);
        this.state = {
            profile: SampleData.result
        }
    }

    componentDidMount() {
        //console.log(SampleData.result);

        // axios.get('https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJd1LPd9jX1IkR5EKFeAdO4Mo&key=AIzaSyAosY_7TUB3yGbpSGrdi1zT4BpLcbK5dOs')
        // .then( response =>{
        //     console.log(response)
        // })
    }

    render() {
        const galleryImages = [
            {
                original: 'http://localhost:3000/images/d1.jpg',
                thumbnail: 'http://via.placeholder.com/250x150',
            },
            {
                original: 'http://localhost:3000/images/d2.jpg',
                thumbnail: 'http://via.placeholder.com/250x150',
            },
            {
                original: 'http://localhost:3000/images/d3.jpg',
                thumbnail: 'http://via.placeholder.com/250x150',
            }
        ]
        return (
            <div>
                <Nav />
                <div id='profile' className='container'>
                    <div className='row'>
                        <div className='col-sm-12 col-md-12' style={{ backgroundColor: '#eee' }}>
                            <br/><br/>
                            <h1>{this.state.profile.name}</h1>
                            <StarRatings
                                rating={this.state.profile.rating}
                                starRatedColor="orange"
                                numberOfStars={5}
                                starDimension='20px'
                                name='rating'
                            />
                            <p className='lead'>
                                {this.state.profile.formatted_address}
                            </p>
                            <FeatureBar
                                website={this.state.profile.website}
                                phone={this.state.profile.international_phone_number}
                            />



                            <br />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra, augue eu pharetra feugiat, dui justo congue massa, at accumsan urna nulla maximus mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra, augue eu pharetra feugiat, dui justo congue massa, at accumsan urna nulla maximus mauris.</p>
                            <p>Sed viverra, augue eu pharetra feugiat, dui justo congue massa, at accumsan urna nulla maximus mauris.</p> 
                            <br /> 
                            <div className='col-sm-12 col-md-12' style={{ backgroundColor: '' }}>
                                <br />
                                <iframe
                                    title="Location"
                                    src={'https://www.google.com/maps/embed/v1/place?key=' + GoogleKeys.GOOGLE_API_KEY + '&q=' +
                                        this.state.profile.name.replace(' ', '+') +
                                        this.state.profile.formatted_address.replace(' ', '+')} style={{ width: '100%', height: '300px', border: 0 }} allowFullScreen></iframe>

                            </div>
                           
                            <Gallery images={galleryImages} />

                            <Review
                                title='Google Reviews'
                                reviews={this.state.profile.reviews} />
                          
                        </div>


                        <p>&nbsp;</p>

                    </div>
                </div>
            </div>

        )
    }
}

export default Individual;