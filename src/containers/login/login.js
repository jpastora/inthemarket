import React, { Component } from 'react';
import {FireBaseKeys} from '../../config/keys';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import profileActions from '../../store/actions/profile'; 
import firebase from 'firebase'; 
import { connect } from 'react-redux';

firebase.initializeApp( FireBaseKeys );



class Login extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount = () =>{
        firebase.auth().onAuthStateChanged( user =>{
            if( user ){ 
                let userInfo = {
                    uid: user.uid,
                    displayName: user.displayName,
                    email: user.email
                };
                this.props.updateLoginInfo(true, userInfo);
            } else{
                this.props.updateLoginInfo(false);
            } 
        })
    }

    render() {    
        const authConfig = {
            signInFlow: 'popup',  
            signInOptions: [
                firebase.auth.EmailAuthProvider.PROVIDER_ID,
                firebase.auth.FacebookAuthProvider.PROVIDER_ID,
                firebase.auth.GoogleAuthProvider.PROVIDER_ID
            ],
            callbacks: {
                signInSuccess: () => false
            }
        }

        return (
            <div>
                {this.props.isSignedIn? (
                    <div className='center'>
                        <br/>
                        <br/>
                        <button onClick={()=>{firebase.auth().signOut()}}>Sign Out</button>
                   
                    </div>
                ):(
                    <span>
                        <StyledFirebaseAuth
                            firebaseAuth={ firebase.auth() }
                            uiConfig={authConfig} /> 
                    </span>
                )} 
            </div>
        )
    }
}
const mapStateToProps = (state) =>{
    return {
        isSignedIn: state.profile.signedIn
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        updateLoginInfo: (status, userInfo) => { dispatch({ type: profileActions.UPDATE_LOGIN, status: status, userInfo: userInfo }) }
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(Login);