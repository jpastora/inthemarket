import React, { Component } from 'react';
import profileActions from '../../store/actions/profile';
import { connect } from 'react-redux'; 
import firebase from 'firebase';
import './nav.css';

class MainMenu extends Component {
    constructor( props ){
        super( props );
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <a className="navbar-brand" href="#">inthemarket.io</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="/">Home <span className="sr-only"></span></a>
                        </li>
                        
                        <li className="nav-item">
                            <a className="nav-link" href="/profile">Profile</a>
                        </li>
                    </ul>
                    <ul className='navbar navbar-expand-lg navbar-dark bt-primary' style={{float:"right"}}>
                    {this.props.displayName?(
                        <li class="nav-item dropdown mr-auto">
                            <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {this.props.displayName}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" className='nav-link' href='#' onClick={ () =>{ firebase.auth().signOut()}} >Sign Out</a> 
                            </div>
                        </li> ):(<span></span>)}
                    </ul>
                </div>
            </nav>
        )
    }
}

const mapStateToProps = state => {
    return {
        displayName: state.profile.displayName
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onProfileLoad: () => { dispatch({ type: profileActions.SHOW_PROFILE }) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);