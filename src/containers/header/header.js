import React from 'react';
import MainMenu from '../navbar/main_menu';

const Header = () => {
    return (
        <header className="App-header">
            <MainMenu />
        </header>
    )
}

export default Header; 