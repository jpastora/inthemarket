import React, { Component } from 'react';  
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom'; 
import Home from '../../pages/home';
import Profile from '../../pages/profile';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div className="App h-100">
          <Route path='/' exact component={Home} /> 
          <Route path='/profile' component={Profile} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;