import React from 'react';
import './textbox.css';

const TextBox = (props) => {
    
    return (
        <div className='input-group'>
            <input type={props.type} className='textbox form-control' 
                style ={ props.style }
                placeholder ={props.placeholder} />
        </div>
    )
}

export default TextBox;