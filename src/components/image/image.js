import React from 'react';

const Img = (props) =>{
    return(
        <span>
            <img
            alt={props.alt}
            src={props.src} />
        </span>
    )
}

export default Img;