import React from 'react';
import './hrtext.css';

const HRText = (props) =>{
    return(
        <hr className='hr-text' data-content={props.text} />
    )
}

export default HRText;