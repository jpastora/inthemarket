import act from '../actions/profile';

const initialState = {
    uid:'',
    signedIn: false, 
    displayName: '',
    email: ''
}

const profileReducer = (state = initialState, action) => {

    switch (action.type) {
        case act.SHOW_PROFILE:
            return { ...state }
            break;
        case act.UPDATE_LOGIN:
            console.log(action.status);
            if(action.status == true){
                console.log('REDUCER', action.userInfo, action.userInfo.email )
                return { ...state, signedIn: action.status, 
                        uid: action.userInfo.uid,
                        displayName: action.userInfo.displayName, email: action.userInfo.email }
            } else {
                return { ...state, signedIn: action.status, displayName: '', email: '' }
            }
            break;
        default:
            return { ...state }
    }
}

export default profileReducer;