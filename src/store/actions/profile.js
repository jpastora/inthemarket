const actions = {
    SHOW_PROFILE: 'SHOW_PROFILE',
    UPDATE_LOGIN: 'UPDATE_LOGIN'
}

export default actions